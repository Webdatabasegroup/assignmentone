// Retailer at minimum has to fill these fields, since he/she has to append these details to effectively communicate with Manufacturer
export interface IRetailer extends Document {
    Company_name: string,   // Ensure if it is not a number
    Country_code: number,
    Phone_Number: number,
    Address: string,
    Country: string,
    City: string,           // Ensure if it is not a number
    Retailer_ID: string,    // Check if it is greater than 3 characters
    Password: string       // Check if it is between length 3 - 16, has some special characters, uppercase, lowercase and a number.
}

// 5 associated unit tests

/*
   Test Password, if it contains length 3 - 16, has some special characters, uppercase, lowercase and a number.
   Test if retailer id is a string and not a number
   Test if city is a string
   Test if company name is a string
   Test if company name, retailer id and password is given or not in a typical usecase.
 */

