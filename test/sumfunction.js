
const numarray = [1,2,3,4,5];
module.exports = function sum(numarray){
    if(!numarray){
        return 0;
    }
   return numarray.reduce((accm, value)=> accm += value, 0);
};



