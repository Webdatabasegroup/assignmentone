
const mongoose = require('mongoose');
const assert = require('chai').assert;
const Retailer = require('../retailerSchema').Retailer;



// I encountered a problem: The problem with that is require statement are importing only the function related with your model so if you want to get the model try this follow statement:


// Test Case
const sample_retailer = new Retailer({Company_name: "Facebook", Country_Code : 1, Phone_Number: 1234567890, Address: "100 Fox Rd", Country: "United States", City: "Seattle", Retailer_ID: "Micro101", password: "Satya@1967"});
const sample_retailer1 = new Retailer({Company_name: "Google", Country_Code : 1, Phone_Number: 9807654321, Address: "102 Jackson Rd", Country: "United States", City: "New York", Retailer_ID: "Macro101", password: "Satya@1967"});
  before(function (done) {
      const db_connect =  mongoose.connect("mongodb://localhost:27017/testdb", {useNewUrlParser: true} );
      const db = mongoose.connection;
      db.on('error', (error)=>{
            done(error)
      });
      db.once('open', function () {
          console.log("We are connected to the database");
          done()
      })
  });

//----------------------------------------------- Testing password -----------------------------------------------------

describe('password', function () {
    it('password should meet certain requirements',  function () {
        const p = sample_retailer.password.length;
        assert.isAbove(p, 3);
        assert.isBelow(p, 16);
        const re = /[a-z]/;
        const re1 = /[A-Z]/;
        const re2 = /[0-9]/;
        const re3 = /[!@#$%^&*]/;
        const t1 = re.test(sample_retailer.password);
        const t2 = re1.test(sample_retailer.password);
        const t3 = re2.test(sample_retailer.password);
        const t4 = re3.test(sample_retailer.password);
        assert.isTrue(t1 && t2 && t3 && t4);
    })
});

// -------------------------------------- Testing if missed any required fields ----------------------------------------

describe('mandatory fields', function () {
    it('Mandatory fields should be given without fail',  function () {

        function checknullorundefined(p){
            return p === null || p === undefined;
        }
        const case1 = checknullorundefined(sample_retailer.Company_name);
        const case2 = checknullorundefined(sample_retailer.Retailer_ID);
        const case3 = checknullorundefined(sample_retailer.password);

        assert.isFalse(case1 || case2 || case3)
    })
});


// ----------------------- Checking Retailer ID length --------------------------------------------

describe("retailer id", function(){
    it("length of retailer id should be greater than 3 ", function(){
        const len = sample_retailer.Retailer_ID.length;
        assert.isAbove(len, 3);
    })
});



//---------------------------- Checking if user id is unique  -------------------------------------
// t and h are just some temporary variables
describe("Database", function () {
    it("Inserting in to database", async function () {
        const t =  await Retailer.find({Retailer_ID: sample_retailer1.Retailer_ID});
        const h = t.length;
        assert.equal(h,0)
    })
});

// ----------------------- City length is greater than 1 --------------------------

describe("City ", function () {
    it("Length of city should be greater than 1", function () {
        const len = sample_retailer.City.length;
        assert.isAbove(len, 1)
    })
});

// ------------------ Check if city is a string -------------------------------

describe("City", function () {
    it("City should be a string", function () {
        assert.isString(sample_retailer.City)
    })
});



//---------------------------------------------------

after(async function(){
    try {
        await mongoose.connection.db.dropDatabase();
    }
    catch (e) {
        console.log(e)
    }
});





