"use strict";
exports.__esModule = true;
var mongoose = require("mongoose");
var orderSchema = new mongoose.Schema({
    //At most medicine ID has 7 digits
    Medicine_ID: {
        type: Number,
        validate: {
            validator: function (medid) {
                return medid > 0 && medid < 9999999;
            }
        },
        required: true
    },
    // Count cannot be less than 0
    count: {
        type: Number,
        validate: {
            validator: function (count) {
                return count > 0;
            }
        },
        required: true
    },
    // Notes length cannot be greater than 500 characters.
    Notes: {
        type: String,
        validate: {
            validator: function (notes) {
                return notes.length < 500;
            }
        }
    }
});
exports.Order = mongoose.model('Order', orderSchema);
