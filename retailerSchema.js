"use strict";
exports.__esModule = true;
/*
Custom validation is declared by passing a validation function.
 */
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var retailerSchema = new mongoose.Schema({
    Company_name: {
        type: String,
        required: true,
        get: obfuscate
    },
    Country_Code: {
        type: Number,
        required: true // Built-In validation
    },
    Phone_Number: {
        type: Number,
        required: true
    },
    Address: {
        type: String,
        required: true
    },
    Country: {
        type: String,
        required: true
    },
    City: {
        type: String,
        validate: {
            validator: function (city) {
                return city.length > 1;
            }
        }
    },
    // Should have at least length of 4 characters
    Retailer_ID: {
        type: String,
        validate: {
            validator: function (id) {
                return id.length >= 4;
            },
            message: "retailer id should have a minimum length of 4"
        },
        required: true,
        unique: true,
        set: convert_lower
    },
    // Password has lot of requirements. Should have an uppercase, lowercase, number and special characters.
    password: {
        type: String,
        validate: {
            validator: function (password) {
                var re = /[a-z]/;
                var re1 = /[A-Z]/;
                var re2 = /[0-9]/;
                var re3 = /[!@#$%^&*]/;
                var t1 = re.test(password);
                var t2 = re1.test(password);
                var t3 = re2.test(password);
                var t4 = re3.test(password);
                return t1 && t2 && t3 && t4 && password.length > 8;
            },
            message: "Password should have length of atleast 8, one uppercase, one lowercase and one number at least"
        },
        required: true
    }
});
// Setter: Making retailer_id to lowercase
function convert_lower(retailer_id) {
    if (!(this instanceof mongoose.Document)) {
        return retailer_id;
    }
    return retailer_id.toLowerCase();
}
// Getter: Obfuscating password
function obfuscate(company_name) {
    if (company_name) {
        return "****";
    }
}
// We have to write code for validators.
retailerSchema.pre('validate', function (next) {
    next();
});
exports.Retailer = mongoose.model('Retailer', retailerSchema);
