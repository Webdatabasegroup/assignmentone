export interface IMedicine extends Document{
    Medicine_name: string,  // check if it is not a number
    count: number,    //check if count is not negative and greater than 0
    Medicine_ID: number // check if id has greater than 3 characters.
}

// 5 associated unit tests

/*

   Test if count is greater than 0
   Test if medicine id, name and count are given. All fields are mandatory
   Test if medicine id is unique
   Test if length of medicine id is greater than 3

 */